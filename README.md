## Rocket Motor Controller   
Rocket Cotor Controller (RMC) is the main avionics board for Cronos rocket, and is mainly responsible for the rocket's motor operation. RMC provides the motor's ignition, operates the valve opening mechanisms and monitors the motor's pressure. It can terminate the motor's operation automatically if it detects problematic conditions. Furthermore, RMC can amplify and read 4 load cell inputs for measuring the engine's thrust during static tests and the run tank's fuel mass. RMC will also activate the DC motors needed for drogue parachute ejection, and heat a piece of nichrome wire in order for the main parachute to be deployed. 

<img src="/images/RMC_v2_0_0_closeup.jpg" alt="RMC v2.0.0" title="RMC v2.0.0" width = "700">

## Libraries 
In order to use this project one should download LSF's KiCad library (https://gitlab.com/librespacefoundation/lsf-kicad-lib.git) 
and configure LSF_KICAD_LIB as an environment variable in KiCad (Preferences > Configure Paths).

## Design reports 
At this point, 2 versions of RMC have been designed and assembled. A mistakes analysis for the first version and a design report for the second can be found [here](https://gitlab.com/groups/librespacefoundation/cronos-rocket/avionics/-/wikis/RMC-v1.0.0-review-and-design-mistakes-analysis) and [here](https://gitlab.com/groups/librespacefoundation/cronos-rocket/avionics/-/wikis/RMC-v2.0.0-design-report) respectively. 
 




